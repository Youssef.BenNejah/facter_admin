import './App.css';
import AddUser from './Components/AddUser';
import AllUsers from './Components/AllUsers';
import Home from './Components/Home';
import NavBar from './Components/NavBar';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import NotFound from './Components/NotFound';
import EditUser from './Components/EditUser';

function App() {
  return (
    <div className="App">
      <BrowserRouter >
        <NavBar />

        <Routes>
          <Route exact path='/'    element={<Home />} />
          <Route exact path='/all' element={<AllUsers />} />
          <Route exact path='/add' element={<AddUser />} />
          <Route exact path='/edit/:id' element={<EditUser />} />
          <Route element={<NotFound />} />



        </Routes>

      </BrowserRouter>


    </div>
  );
}

export default App;
