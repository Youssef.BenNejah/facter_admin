import express from 'express' ;
import mongoose from 'mongoose';
import route from './route/route.js';
import cors from'cors';
import bodyParser from 'body-parser';

const app =express () ;


app.use(bodyParser.json({extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use('/users', route);


const PORT = 8000 ;
const URL = "mongodb+srv://admin:admin@adminfacter.r0rwc.mongodb.net/adminFacter?retryWrites=true&w=majority"

mongoose.connect(URL, { useNewUrlParser: true, }).then(() => { 
    
    app.listen(PORT, () => console.log(`Server is running on PORT: ${PORT}`))
}).catch((error) => {
    console.log('Error:', error.message)
})